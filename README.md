## Configuring GitLab Runner using the Helm Chart
Create a `values.yaml` file for your GitLab Runner configuration. See [Helm docs](https://github.com/kubernetes/helm/blob/master/docs/chart_template_guide/values_files.md) for information on how your values file will override the defaults.

The default configuration can always be found in the `values.yaml` in the chart repository.

### Required configuration
In order for GitLab Runner to function, your config file **must** specify the following:

- `gitlabUrl` - the GitLab server full URL (e.g., https://example.gitlab.com) to register the Runner against.
- `runnerRegistrationToken` - The registration token for adding new Runners to GitLab. This must be [retrieved from your GitLab instance](https://docs.gitlab.com/ee/ci/runners/).
Unless you need to specify any additional configuration, you are ready to **install the Runner**.


## Installing GitLab Runner using the Helm Chart

Add the GitLab Helm repository and initialize Helm:

```
helm repo add gitlab https://charts.gitlab.io
helm init
```

Once you have configured GitLab Runner in your values.yml file, run the following:
```
helm install --namespace <NAMESPACE> --name gitlab-runner -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
````

Where:

- `<NAMESPACE>` is the Kubernetes namespace where you want to install the GitLab Runner.
- `<CONFIG_VALUES_FILE>` is the path to values file containing your custom configuration. See the Configuring GitLab Runner using the Helm Chart section to create it.


```
helm install --namespace runners --name gitlab-runner -f values.yaml gitlab/gitlab-runner
```

## Delete GitLab Runner
```bash
helm del --purge gitlab-runner;
```